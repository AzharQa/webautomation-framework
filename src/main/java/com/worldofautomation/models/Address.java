package com.worldofautomation.models;
import lombok.Data;


@Data
public class Address {

    public Address() { //default

    }


    private int employee_id;
    private String street;
    private String apt;
    private String city;
    private int zipcode;
    private String state;

    public Address(int employee_id, String street, String apt, String city, int zipcode, String state) {
        this.employee_id = employee_id;
        this.street = street;
        this.apt = apt;
        this.city = city;
        this.zipcode = zipcode;
        this.state = state;
    }




}
