package com.worldofautomation.restapitests.models;

public class Address {
    public Address(int employee_id, String street, String apt, String city, int zipcode, String state) {
    }

    public Address(){} //default constructor

    private int employee_id;
    private String street;
    private String apt;
    private String city;
    private int zipcode;
    private String state;

    @Override
    public String toString() {
        return "Address{" +
                "employee_id=" + employee_id +
                ", street='" + street + '\'' +
                ", apt=" + apt +
                ", city=" + city +
                ", zipcode='" + zipcode + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
    public int getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getApt() {
        return apt;
    }

    public void setApt(String apt) {
        this.apt = apt;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getZipcode() {
        return zipcode;
    }

    public void setZipcode(int zipcode) {
        this.zipcode = zipcode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }




}
