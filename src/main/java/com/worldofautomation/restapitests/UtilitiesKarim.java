package com.worldofautomation.restapitests;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class UtilitiesKarim {
//    public static void main(String[] args) {
//
//        JSONArray jsonArray = getJSONArray("src/test/resources/TestData.json");
//        System.out.println(jsonArray);
//        JSONObject jsonObject = (JSONObject) jsonArray.get(1);
//        System.out.println(jsonObject);
//        System.out.println(jsonObject.get("searchData"));
//    }

    public static JSONArray getJSONArray(String filepath) {
        JSONParser jsonParser = new JSONParser();
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(filepath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        JSONArray jsonArray = null;
        try {
            jsonArray = (JSONArray) jsonParser.parse(fileReader);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return jsonArray;
    }
    }



        //or

        /*
        JsonParser jsonParser = new JsonParser();
        FileReader fileReader = new FileReader(""src/test/resources/testData.json");
        JsonArray jsonArray = jsonParser.parse(fileReader);
         */

