package com.worldofautomation.restapitests;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestBase {

    private static WebDriver driver = null;
    private static ExtentReports extent;

    @Parameters({"url", "browserName", "cloud", "os"})
    @BeforeMethod
    public static void testConfig(String url, String browserName, boolean cloud, String osName) {

        if (cloud == true) {
            String username = "azharkautomation1";
            String automate_Key = "mFTz1sSxLoaBGPBJpFPQ";  //camelcasing
            String urlForBrowserStack = "https://" + username + ":" + automate_Key + "@hub-cloud.browserstack.com/wd/hub";

            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("browser", "Chrome");
            caps.setCapability("browser_version", "81.0");
            caps.setCapability("os", "OS X");
            caps.setCapability("os_version", "Catalina");
            caps.setCapability("resolution", "1024x768");
            caps.setCapability("name", "Cloud Testing Execution");

            URL url1 = null;
            try {
                url1 = new URL(urlForBrowserStack);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            driver = new RemoteWebDriver(url1, caps);
        } else {
            if (osName.equalsIgnoreCase("mac")) {
                if (browserName.equalsIgnoreCase("chrome")) {
                    System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
                    driver = new ChromeDriver();

                } else if (browserName.equalsIgnoreCase("mozilla")) {
                    System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");

                    // object for Firefox Mozilla driver
                    driver = new FirefoxDriver();
                }

                driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
                driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                driver.get(url);

            } else {
                if (browserName.equalsIgnoreCase("chrome")) {
                    System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
                    driver = new ChromeDriver();


                } else if (browserName.equalsIgnoreCase("mozilla")) {
                    System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");

                    // object for Firefox Mozilla driver
                    driver = new FirefoxDriver();
                }

            }
        }
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.getCurrentUrl();
    }

    //*********************** EXTENT STARTS HERE **************************************

    //method#2 WaitFor(time in seconds);
    public static void waitFor(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //method#3 Exit Browser Completely closing all tabs
    public static void exitBrowser() {
        driver.quit();
    }


    //method#4 close Browser Tab but not exit completely
    public static void closeBrowserTab() {
        driver.close();
    }

    //method #5 clickByElementId .. perform click on the button with specified id
    public static void clickByElementId(String buttonId) {
        driver.findElement(By.id(buttonId)).click();
    }
    //************** EXTENT FINISHED ************************

    //method #6 sendKeysUsingId
    public static void sendKeysUsingId(String id, String fieldValue) {
        driver.findElement(By.id(id)).sendKeys(fieldValue);
    }

    //method #7 clickByXpath
    public static void clickByElementXpath(String xPath) {
        driver.findElement(By.xpath(xPath)).click();
    }

    //#8
    public static boolean validateXpathIsDisplayed(String xpath) {

        boolean ifDisplayed = driver.findElement(By.xpath(xpath)).isDisplayed();
        return ifDisplayed;

    }

    //9
    public static boolean validateListOfXpathIsDisplayed(String xpath) {
        List<WebElement> elementList = driver.findElements(By.xpath(xpath));
        boolean flag = false;

        for (int i = 0; i < elementList.size(); i++) {

            // if displayed will throw exception if the element is not the in the page
            // no such element found exception

            if (elementList.get(i).isDisplayed()) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    //10
    public static String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    //Method 11 Validate a text is Displayed
    public static boolean validateifXpathisDisplayed(String Xpath) {
        boolean ifDisplayed = driver.findElement(By.xpath(Xpath)).isDisplayed();
        return ifDisplayed;

    }


    //#12 gettextFromXpath
    public static String getTextFromXpath(String Xpath) {
        String text = driver.findElement(By.xpath(Xpath)).getText();
        return text;
    }

    //#13 Validate ElementList from List
    public static boolean validateListOfXpathDisplayed(String Xpath) {
        List<WebElement> elementList = driver.findElements(By.xpath(Xpath));
        boolean flag = false;

        for (int i = 0; i <= elementList.size(); i++) {
            //no such element found exception is thrown if element is not found in the page
            if (elementList.get(i).isDisplayed()) {
                System.out.println(i);
                flag = true;
                break;
            }
        }
        return flag;
    }


    public static WebDriver getDriver() {
        return driver;
    }

    //Method14 ClickbyLinkText
    public static void clickByLinkText(String Text) {
        driver.findElement(By.linkText(Text)).click();
    }

    @AfterMethod
    public static void closeBrowser() {
        waitFor(5);
        driver.quit();
    }

    @BeforeSuite(alwaysRun = true)
    public void extentSetup(ITestContext context) {
        ExtentTestManager.setOutputDirectory(context);
        extent = ExtentTestManager.getInstance();
    }

    //Extent Report Setup for each methods
    @BeforeMethod(alwaysRun = true)
    public void startExtent(Method method) {
        String className = method.getDeclaringClass().getSimpleName();
        ExtentTestManager.startTest(method.getName());
        ExtentTestManager.getTest().assignCategory(className);
    }

    //Extent Report cleanup for each methods
    @AfterMethod(alwaysRun = true)
    public void afterEachTestMethod(ITestResult result) {
        ExtentTestManager.getTest().getTest().setStartedTime(ExtentTestManager.getTime(result.getStartMillis()));
        ExtentTestManager.getTest().getTest().setEndedTime(ExtentTestManager.getTime(result.getEndMillis()));
        for (String group : result.getMethod().getGroups()) {
            ExtentTestManager.getTest().assignCategory(group);
        }

        if (result.getStatus() == 1) {
            ExtentTestManager.getTest().log(LogStatus.PASS, "TEST CASE PASSED : " + result.getName());
        } else if (result.getStatus() == 2) {
            ExtentTestManager.getTest().log(LogStatus.FAIL, "TEST CASE FAILED : " + result.getName() + " :: " + ExtentTestManager.getStackTrace(result.getThrowable()));
        } else if (result.getStatus() == 3) {
            ExtentTestManager.getTest().log(LogStatus.SKIP, "TEST CASE SKIPPED : " + result.getName());
        }
        ExtentTestManager.endTest();
        extent.flush();
        if (result.getStatus() == ITestResult.FAILURE) {
            ExtentTestManager.captureScreenshot(driver, result.getName());
        }
    }

    //Extent Report closed
    @AfterSuite(alwaysRun = true)
    public void generateReport() {
        extent.close();
    }

    //method15
    public class OSValidator {

        private String OS = System.getProperty("os.name").toLowerCase();

        public void main(String[] args) {

            System.out.println(OS);

            if (isWindows()) {
                System.out.println("This is Windows");
            } else if (isMac()) {
                System.out.println("This is Mac");
            } else {
                System.out.println("Your OS is not supported!!");
            }
        }

        public boolean isWindows() {

            return (OS.indexOf("win") >= 0);

        }

        public boolean isMac() {

            return (OS.indexOf("mac") >= 0);


        }
//method16
        public boolean validateWebelement(WebElement element, String data) {
            if (element.getText().equalsIgnoreCase(data)) {
                return true;
            } else {
                return false;
            }

        }
    }
}

