package com.worldofautomation.restapitests.restapis;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.worldofautomation.restapitests.ConnectSql;
import com.worldofautomation.restapitests.models.Address;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AddressTests {

//    //http://localhost:8090/employees
//
//
//    @Test
//    public void getAllEmployeesAddress() throws JsonProcessingException, SQLException {
//        RestAssured.baseURI = "http://localhost:8090/address";
//
//        Response response = RestAssured.given().when().log().all().get("/all")
//                .then().assertThat().statusCode(200).extract().response();
//
//        // storing all the data into a list of class
//        ObjectMapper objectMapper = new ObjectMapper();
//        //List<Employee> employee= objectMapper.readValue(response.asString(),List.class); --> recommended. but doesn't give inside on individual employee
//
//        List<Address> employeeFromAPI = objectMapper.readValue(response.asString(), objectMapper.getTypeFactory().constructCollectionType(List.class, Address.class));
//        // this gives all the inside of the employee/the class you are creating obj of
//
//
//        System.out.println(employeeFromAPI.get(0));
//        System.out.println(employeeFromAPI.get(1));
//
//        Address tanbir = employeeFromAPI.get(0);
//        System.out.println(tanbir.getCity());
//        System.out.println(tanbir.getStreet());
//
//
//        // storing all the data from db into a list of class
//
//        Connection connection = ConnectSql.getConnection("worldOfAutomation");
//        Statement statement = ConnectSql.getStatement(connection);
//        ResultSet resultSet = ConnectSql.getResultSet(statement, "SELECT * FROM worldOfAutomation.employee;");
//        List<Address> employeeFromDB = new ArrayList<>();
//
//        while (resultSet.next()) {
//            int employee_id = resultSet.getInt("employee_id");
//            String street = resultSet.getString("street");
//            String apt = resultSet.getString("apt");
//            String city = resultSet.getString("city");
//            int zipcode = resultSet.getInt("zipcode");
//            String state = resultSet.getString("state");
//            Address address = new Address(employee_id, street, apt, city, zipcode, state);
//            employeeFromDB.add(address);
//        }
//
//
//        // validate the both data
//
//        System.out.println(employeeFromAPI.toString());
//        System.out.println(employeeFromDB.toString());
//
//        Assert.assertEquals(employeeFromAPI.toString(), employeeFromDB.toString());
//
//
//    }
//
//
//    @Test
//    public void getSingleEmployeeAddress() throws JsonProcessingException, SQLException {
//        RestAssured.baseURI = "http://localhost:8090/employees";
//
//        Response response = RestAssured.given().when().log().all().get("/1")
//                .then().assertThat().statusCode(200).extract().response();
//
//
//        ObjectMapper objectMapper = new ObjectMapper();
//        Address addressFromApi = objectMapper.readValue(response.asString(), Address.class);
//
//        // get the data from db
//        Connection connection = ConnectSql.getConnection("worldOfAutomation");
//        Statement statement = ConnectSql.getStatement(connection);
//        ResultSet resultSet = ConnectSql.getResultSet(statement, "SELECT * FROM worldOfAutomation.employee where id=1;");
//        Address addressFromDB = null;
//        while (resultSet.next()) {
//            int employee_id = resultSet.getInt("employee_id");
//            String street = resultSet.getString("street");
//            String apt = resultSet.getString("apt");
//            String city = resultSet.getString("city");
//            int zipcode = resultSet.getInt("zipcode");
//            String state = resultSet.getString("state");
//            addressFromDB = new Address(employee_id, street, apt, city, zipcode, state);
//        }
//
//        System.out.println(addressFromDB.toString());
//        System.out.println(addressFromApi.toString());
//
//        // validate data is on the db
//
//        Assert.assertEquals(addressFromApi.toString(), addressFromDB.toString());
//    }
//
//
//    @Test
//    public void deleteSingleEmployeeAddress() throws SQLException {
//        RestAssured.baseURI = "http://localhost:8090/employees";
//
//        Response response = RestAssured.given().when().log().all().delete("/delete/2")
//                .then().assertThat().statusCode(204).extract().response();
//
//        // get the data from db
//        Connection connection = ConnectSql.getConnection("worldOfAutomation");
//        Statement statement = ConnectSql.getStatement(connection);
//        ResultSet resultSet = ConnectSql.getResultSet(statement, "SELECT * FROM worldOfAutomation.employee where id=2;");
//
//        //make sure its empty
//
//       /* while (!resultSet.next()){
//            System.out.println("resultset is empty");
//            break;
//        }*/
//
//        if (!resultSet.next()) {
//            System.out.println("resultset is empty");
//        } else {
//            Assert.fail();
//        }
//
//    }
//
//
//    @Test
//    public void insertAnEmployeeAddress() {
//        RestAssured.baseURI = "http://localhost:8090/employees";
//
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("employee_id", "1");
//        jsonObject.put("street", "8156 102nd Ave");
//        jsonObject.put("apt", "1FL");
//        jsonObject.put("city", "Queens");
//        jsonObject.put("zipcode",11434);
//        jsonObject.put("state", "NY");
//
//        JSONObject jsonObject2 = new JSONObject();
//        jsonObject.put("employee_id", "2");
//        jsonObject.put("street", "123 Sesame Pl");
//        jsonObject.put("apt", "Gfl");
//        jsonObject.put("city", "Hamburg");
//        jsonObject.put("zipcode",11234);
//        jsonObject.put("state", "TX");
//
//        JSONArray jsonArray = new JSONArray();
//        jsonArray.add(jsonObject);
//        jsonArray.add(jsonObject2);
//
//
//        Response response = RestAssured.given().header("Content-Type", "application/json")
//                .when().body(jsonArray).log().all().post("/add")
//
//                .then().assertThat().statusCode(200).extract().response();
//
//        System.out.println(response.body().prettyPrint());
//
//        // get the data from db
//
//        // SELECT * FROM worldOfAutomation.employee where name in ('Rohan','Md');
//
//        // check the both data
//
//    }
//
//
//    @Test
//    public void updateAEmployee() throws SQLException {
//        RestAssured.baseURI = "http://localhost:8090/employees";
//
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("employee_id", "1");
//        jsonObject.put("zipcode", 11434);
//
//        Response response = RestAssured.given().header("Content-Type", "application/json")
//                .body(jsonObject.toString()).log().all().when().put("/update/8")
//                .then().assertThat().statusCode(200).extract().response();
//
//        System.out.println(response.body().prettyPrint());
//
//        Connection connection = ConnectSql.getConnection("worldOfAutomation");
//        Statement statement = ConnectSql.getStatement(connection);
//        ResultSet resultSet = ConnectSql.getResultSet(statement, "SELECT * FROM worldOfAutomation.employee where id =8;");
//        Address addressFromDB = new Address(employee_id, street, apt, city, zipcode, state);
//
//        while (resultSet.next()) {
//            int id = resultSet.getInt("id");
//            String name = resultSet.getString("name");
//            boolean permanent = resultSet.getBoolean("permanent");
//            int phone_number = resultSet.getInt("phone_number");
//            String role = resultSet.getString("role");
//            String city = resultSet.getString("city");
//            addressFromDB  = new Address(employee_id, street, apt, city, zipcode, state);
//        }
//
//        System.out.println(addressFromDB.toString());
//        Assert.assertFalse(addressFromDB.isPermanent());
//    }


}
