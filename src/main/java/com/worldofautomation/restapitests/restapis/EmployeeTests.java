package com.worldofautomation.restapitests.restapis;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.worldofautomation.models.Employee;
import com.worldofautomation.restapitests.ConnectSql;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class EmployeeTests {

    //http://localhost:8090/employees


    @Test
    public void getAllTheEmployees() throws JsonProcessingException, SQLException {
        RestAssured.baseURI = "http://localhost:8090/employees";

        Response response = RestAssured.given().when().log().all().get("/all")
                .then().assertThat().statusCode(200).extract().response();

        // storing all the data into a list of class
        ObjectMapper objectMapper = new ObjectMapper();
        //List<Employee> employee= objectMapper.readValue(response.asString(),List.class); --> recommended. but doesn't give inside on individual employee

        List<Employee> employeeFromAPI = objectMapper.readValue(response.asString(), objectMapper.getTypeFactory().constructCollectionType(List.class, Employee.class));
        // this gives all the inside of the employee/the class you are creating obj of


        System.out.println(employeeFromAPI.get(0));
        System.out.println(employeeFromAPI.get(1));

        Employee tanbir = employeeFromAPI.get(0);
        System.out.println(tanbir.getCity());
        System.out.println(tanbir.getPhone_number());


        // storing all the data from db into a list of class

        Connection connection = ConnectSql.getConnection("worldOfAutomation");
        Statement statement = ConnectSql.getStatement(connection);
        ResultSet resultSet = ConnectSql.getResultSet(statement, "SELECT * FROM worldOfAutomation.employee;");
        List<Employee> employeeFromDB = new ArrayList<>();

        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            boolean permanent = resultSet.getBoolean("permanent");
            int phone_number = resultSet.getInt("phone_number");
            String role = resultSet.getString("role");
            String city = resultSet.getString("city");
            Employee employee = new Employee(id, name, permanent, phone_number, role, city);
            employeeFromDB.add(employee);
        }


        // validate the both data

        System.out.println(employeeFromAPI.toString());
        System.out.println(employeeFromDB.toString());

        Assert.assertEquals(employeeFromAPI.toString(), employeeFromDB.toString());


    }


    @Test
    public void getSingleEmployee() throws JsonProcessingException, SQLException {
        RestAssured.baseURI = "http://localhost:8090/employees";

        Response response = RestAssured.given().when().log().all().get("/1")
                .then().assertThat().statusCode(200).extract().response();


        ObjectMapper objectMapper = new ObjectMapper();
        Employee employeeFromAPI = objectMapper.readValue(response.asString(), Employee.class);

        // get the data from db
        Connection connection = ConnectSql.getConnection("worldOfAutomation");
        Statement statement = ConnectSql.getStatement(connection);
        ResultSet resultSet = ConnectSql.getResultSet(statement, "SELECT * FROM worldOfAutomation.employee where id=1;");
        Employee employeeFromDB = null;
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            boolean permanent = resultSet.getBoolean("permanent");
            int phone_number = resultSet.getInt("phone_number");
            String role = resultSet.getString("role");
            String city = resultSet.getString("city");
            employeeFromDB = new Employee(id, name, permanent, phone_number, role, city);
        }

        System.out.println(employeeFromDB.toString());
        System.out.println(employeeFromAPI.toString());

        // validate data is on the db

        Assert.assertEquals(employeeFromAPI.toString(), employeeFromDB.toString());
    }


    @Test
    public void deleteSingleEmployee() throws SQLException {
        RestAssured.baseURI = "http://localhost:8090/employees";

        Response response = RestAssured.given().when().log().all().delete("/delete/2")
                .then().assertThat().statusCode(204).extract().response();

        // get the data from db
        Connection connection = ConnectSql.getConnection("worldOfAutomation");
        Statement statement = ConnectSql.getStatement(connection);
        ResultSet resultSet = ConnectSql.getResultSet(statement, "SELECT * FROM worldOfAutomation.employee where id=2;");

        //make sure its empty

       /* while (!resultSet.next()){
            System.out.println("resultset is empty");
            break;
        }*/

        if (!resultSet.next()) {
            System.out.println("resultset is empty");
        } else {
            Assert.fail();
        }

    }


    @Test
    public void createAEmployee() {
        RestAssured.baseURI = "http://localhost:8090/employees";

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "Rohan");
        jsonObject.put("permanent", true);
        jsonObject.put("phone_number", 56789);
        jsonObject.put("role", "dev");
        jsonObject.put("city", "bx");

        JSONObject jsonObject2 = new JSONObject();
        jsonObject2.put("name", "Md");
        jsonObject2.put("permanent", true);
        jsonObject2.put("phone_number", 5456);
        jsonObject2.put("role", "dev");
        jsonObject2.put("city", "bx");

        JSONArray jsonArray = new JSONArray();
        jsonArray.add(jsonObject);
        jsonArray.add(jsonObject2);


        Response response = RestAssured.given().header("Content-Type", "application/json")
                .when().body(jsonArray).log().all().post("/add")

                .then().assertThat().statusCode(200).extract().response();

        System.out.println(response.body().prettyPrint());

        // get the data from db

        // SELECT * FROM worldOfAutomation.employee where name in ('Rohan','Md');

        // check the both data

    }


    @Test
    public void updateAEmployee() throws SQLException {
        RestAssured.baseURI = "http://localhost:8090/employees";

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "Sami");
        jsonObject.put("permanent", false);

        Response response = RestAssured.given().header("Content-Type", "application/json")
                .body(jsonObject.toString()).log().all().when().put("/update/8")
                .then().assertThat().statusCode(200).extract().response();

        System.out.println(response.body().prettyPrint());

        Connection connection = ConnectSql.getConnection("worldOfAutomation");
        Statement statement = ConnectSql.getStatement(connection);
        ResultSet resultSet = ConnectSql.getResultSet(statement, "SELECT * FROM worldOfAutomation.employee where id =8;");
        Employee employeeFromDB = new Employee();

        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            boolean permanent = resultSet.getBoolean("permanent");
            int phone_number = resultSet.getInt("phone_number");
            String role = resultSet.getString("role");
            String city = resultSet.getString("city");
            employeeFromDB = new Employee(id, name, permanent, phone_number, role, city);
        }

        System.out.println(employeeFromDB.toString());
        Assert.assertFalse(employeeFromDB.isPermanent());
    }


}
