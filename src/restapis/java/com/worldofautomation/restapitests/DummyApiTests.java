package com.worldofautomation.restapitests;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.Test;

public class DummyApiTests {
    //http://dummy.restapiexample.com/


    @Test
    public void getAllEmployees() {
        RestAssured.baseURI = "http://www.dummy.resapiexample.com/api/v1";

      Response response = RestAssured.given().when().log().all().get("/employees")
                .then().assertThat().statusCode(200).extract().response();
        System.out.println(response.body().prettyPrint());
    }

    @Test
    public void deleteAllEmployees() {
        RestAssured.baseURI = "http://www.dummy.resapiexample.com/api/v1";

        Response response = RestAssured.given().when().log().all().get("/delete/84")
                .then().assertThat().statusCode(200).extract().response();
        System.out.println(response.body().prettyPrint());
    }


}
