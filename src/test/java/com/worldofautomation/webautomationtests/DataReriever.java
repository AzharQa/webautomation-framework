package com.worldofautomation.webautomationtests;

import com.worldofautomation.restapitests.UtilitiesKarim;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.annotations.DataProvider;

public class DataReriever {

    @DataProvider(name="getSearchData")
    public Object[] getSearchData(){
        JSONArray jsonArray = UtilitiesKarim.getJSONArray("src/test/resources/TestData.json");
        JSONObject jsonObject = (JSONObject) jsonArray.get(0);
        String searchkeywords = (String) jsonObject.get("searchkeywords");
        Object[] objects = new Object[1];
      // change values for running test multiples times using dataprovider annotation
        //  Object[] objects = new Object[4];
        objects[0]=searchkeywords;
//        objects[1]="java";
//        objects[2]="Api";
//        objects[3]="Appium";
//        objects[4]="another data to be injected";
        return objects;
    }
}
