package com.worldofautomation.webautomationtests.tests;
import com.worldofautomation.restapitests.ConnectSql;
import com.worldofautomation.restapitests.ExtentTestManager;
import com.worldofautomation.restapitests.TestBase;
import com.worldofautomation.webautomationtests.DataReriever;
import com.worldofautomation.webautomationtests.Queries;
import com.worldofautomation.webautomationtests.pages.HomePage;
import com.worldofautomation.webautomationtests.pages.RegisterPage;
import com.worldofautomation.webautomationtests.pages.SearchResultPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class HomepageFunctionalityTests extends TestBase {
    private HomePage homePage;
    private RegisterPage registerPage;
    private SearchResultPage searchResultPage;

    @BeforeMethod
    private void setupInstances() {
        homePage = PageFactory.initElements(getDriver(), HomePage.class);
        registerPage = PageFactory.initElements(getDriver(), RegisterPage.class);
        searchResultPage = PageFactory.initElements(getDriver(), SearchResultPage.class);
    }


    @Test (dataProviderClass = DataReriever.class, dataProvider = "getSearchData")
    public void userBeingAbletoSearchItemByText(String searchkeywords) {
        homePage.validateUserIsOnHomePage();
        homePage.typeOnSearchBar(searchkeywords);
        homePage.clickOnSearchButton();
        searchResultPage.validateUserIsOnSearchResultPage(searchkeywords);
    }


    @Test(enabled = false)
    public void validateUserIsAbleToLoginUsingValidCreds() {
        homePage.validateUserIsOnHomePage();
        homePage.clickOnRegisterOnButton();
        ExtentTestManager.log("clicked on register button");
        registerPage.fillFnameAndLname();
        ExtentTestManager.log("filled up first name and last name");
    }
    @Test(dataProviderClass = DataReriever.class, dataProvider = "getSearchData", enabled = false)
    public void userBeingAbleToSearchForAnyItemOfTheirPreference(String data) {
        homePage.validateUserIsOnHomePage();
        homePage.typeOnSearchBar(data);
        homePage.clickOnSearchButton();
        searchResultPage.validateUserIsOnSearchResultPage(data);
    }



    @Test(enabled = false)
    public void validateUserIsAbleToSeeSearchGhostText() {
        homePage.validateUserIsOnHomePage();
        homePage.validateGhostTextInSearBox();
    }

    @Test (enabled = false)
    public void userBeingAbleToSearchForAnyItemOfTheirPreferenceDB() throws SQLException {
        Connection connection = ConnectSql.getConnection("worldOfAutomation");
        Statement statement = ConnectSql.getStatement(connection);
        ResultSet resultSet = ConnectSql.getResultSet(statement, Queries.SELECT_QUERY);

        ArrayList<String> booksList = new ArrayList<>();
        while (resultSet.next()) {
            booksList.add(resultSet.getString("Books"));
        }

        System.out.println(booksList);
        homePage.validateUserIsOnHomePage();
        homePage.typeOnSearchBar(booksList.get(0));
        homePage.clickOnSearchButton();
        searchResultPage.validateUserIsOnSearchResultPage(booksList.get(0));
    }
}