package com.worldofautomation.webautomationtests.tests;
import com.worldofautomation.restapitests.ExtentTestManager;
import com.worldofautomation.restapitests.TestBase;
import com.worldofautomation.webautomationtests.pages.HomePage;
import com.worldofautomation.webautomationtests.pages.RegisterPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
public class RegisterPageTest extends TestBase {

    private RegisterPage registerPage;
    private HomePage homePage;  //private variable declaration

    @BeforeMethod void setupInstances(){
        registerPage = PageFactory.initElements(getDriver(), RegisterPage.class);
       homePage = PageFactory.initElements(getDriver(), HomePage.class);
    }

    @Test // (enabled = false)
    public void validateUserIsAbleToRegisterUsingValidCredentials() {
        homePage.validateUserIsOnHomePage();
        homePage.clickOnRegisterOnButton();
        ExtentTestManager.log("Clicked On Sign in Button");
        registerPage.fillUserData();
        ExtentTestManager.log("Entered test user data");
    }
}
