package com.worldofautomation.webautomationtests.pages;
import com.worldofautomation.restapitests.ExtentTestManager;
import com.worldofautomation.restapitests.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class HomePage {

    @FindBy(linkText = "register")
    private WebElement registerBtn;

    @FindBy(id = "gh-ac")
    private WebElement searchBar;

    @FindBy(id = "gh-btn")
    private WebElement searchBtn;

    public void clickOnRegisterOnButton() {
        registerBtn.click();
    }

    public void validateUserIsOnHomePage() {
        String currentUrl = TestBase.getDriver().getCurrentUrl();
        String expectedUrl = "https://www.ebay.com/";
        Assert.assertEquals(currentUrl, expectedUrl);
        ExtentTestManager.log("url has been validated");
    }

    public void typeOnSearchBar(String data) {
        searchBar.sendKeys(data);
        ExtentTestManager.log("Java Books has been typed");
    }

    public void clickOnSearchButton() {
        searchBtn.click();
        ExtentTestManager.log("search button has been clicked");
    }

    public void validateGhostTextInSearBox() {
        String ghostText = searchBar.getAttribute("aria-label");
        Assert.assertEquals(ghostText, "Search for anything");
        Assert.assertTrue(searchBar.isDisplayed());
        ExtentTestManager.log("Search for anything : Ghost text has been displayed");
    }
}