package com.worldofautomation.webautomationtests.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignIn {

    @FindBy(id = "gh-ug")
    private WebElement email;
    @FindBy(id = "signin-continue-btn")
    private WebElement continuebutton;


    public void filledUserName() {
        email.sendKeys("test@gmail.com");
        continuebutton.click();

    }
}

