package com.worldofautomation.webautomationtests.pages;
import com.worldofautomation.restapitests.ExtentTestManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResultPage {

    @FindBy (xpath = "//h1[@class='srp-controls__count-heading']")
    private  WebElement resultForValidation;

    public void validateUserIsOnSearchResultPage(String searchkeywords) {

        String result = resultForValidation.getText();
        System.out.println(result);
//        Assert.assertTrue(result.contains(" Result for "+searchkeywords));
        ExtentTestManager.log("results for ["+searchkeywords+"] has been Displayed");

    }



}
