package com.worldofautomation.webautomationtests.pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterPage {

    @FindBy(id = "firstname")
    private WebElement firstname;
    @FindBy(id = "lastname")
    private WebElement lastnme;
    @FindBy(id = "email")
    private WebElement email;


    public void fillFnameAndLname() {
        firstname.sendKeys("Azhar");
        lastnme.sendKeys("Karim");
        email.sendKeys("testuser@email.com");
    }

    public void fillUserData() {
    }
}


